import React, { useEffect } from "react";
import { render } from "react-dom";

import './style/main.sass';

const App = () => {
  useEffect(() => {
    (async () => {
      const {test} = await import('./test');
      test();
    })();
  });
  return <h1>APP</h1>;
};

const root = document.querySelector('[data-role="root"]');

render(<App />, root);
