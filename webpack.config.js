const { resolve } = require('path');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');

const DIST_DIR = resolve(__dirname, 'dist');

module.exports = (_, { mode }) => {
  const isProd = mode === 'production';

  return {
    target: 'web',
    entry: {
      main: resolve(__dirname, 'src', 'main.ts'),
    },
    devtool: isProd ? false : 'source-map',
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: [resolve(__dirname, 'node_modules')],
        },
        {
          test: /\.sass$/i,
          use: [
            // extract styles into different file
            MiniCssExtractPlugin.loader,
            // Translates CSS into CommonJS
            'css-loader',
            // autoprefixer
            'postcss-loader',
            // Compiles Sass to CSS
            'sass-loader',
          ],
        },
      ],
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js'],
      alias: {
        '@src': './src',
      },
    },
    devServer: {
      port: 8000,
      https: true,
      liveReload: false,
      client: {
        overlay: {
          warnings: false,
          errors: true,
        },
      },
      hot: true,      
      open: ['/dist/index.html']
    },
    output: {
      filename: isProd ? '[name].[contenthash].js' : '[name].js',
      path: DIST_DIR,
      chunkFilename: isProd ? 'chunks/[name].[contenthash].chunk.js' : 'chunks/[name].chunk.js',
      publicPath: '/dist/',
    },
    plugins: [
      new MiniCssExtractPlugin(),
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        filename: './index.html',
        template: './src/index.html',
        directory: DIST_DIR,
      }),
      new ModuleFederationPlugin({
        name: 'main',
        library: { type: 'var', name: 'main' },
        filename: 'export.js',
        exposes: {
        },
        shared: ['react', 'react-dom', '@mui/material', '@emotion/styled', '@emotion/react']
      }),
      // new BundleAnalyzerPlugin(),
    ],
  };
};
